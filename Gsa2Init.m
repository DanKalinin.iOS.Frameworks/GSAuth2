//
//  Gsa2Init.m
//  GSAuth2
//
//  Created by Dan on 26.10.2021.
//

#import "Gsa2Init.h"

@implementation Gsa2Init

+ (void)initialize {
    (void)NseInit.class;
    (void)SqleInit.class;
    (void)SoeInit.class;
    (void)JseInit.class;
    (void)JwtInit.class;
    (void)TlsInit.class;
    (void)GneInit.class;
    
    gsa2_init();
}

@end
