# GSAuth2

GSLabs SmartHome authorization SDK for iOS.

- [Installation](#installation)
- [API](#api)
    - [Database](#database)
    - [Client](#client)
- [Examples](#examples)
    - [Preparation](#preparation)
    - [Login](#login)
    - [Account synchronization](#account-synchronization)
    - [Domain creation](#domain-creation)
    - [Guest invitation](#guest-invitation)
- [Screenshots](#screenshots)
- [TODO](#todo)

## Installation

Add these lines to your `Podfile` and run `pod install` command.

```ruby
pod "GSAuth2", :git => "https://gitlab.com/DanKalinin.iOS.Frameworks/GSAuth2.git", :tag => "1.0"
pod "FoundationExt", :git => "https://gitlab.com/DanKalinin.iOS.Frameworks/FoundationExt.git", :tag => "1.0"
```

## API

The API consists of 2 main interfaces - Database and Client.

### Database

`Gsa2Database` class provides methods to fetch the [entities](images/entities.png) loaded by Client - Accounts, Domains, Controllers, etc. This data can be mapped directly to UI.

### Client

`Gsa2Client` class provides high level API for making backend [requests](https://smart-home-cloud.test.gs-labs.tv/swagger) - GetAccount, CreateDomain, AddControllers, etc. Upon request completion it stores the received [entities](images/entities.png) to Database and invokes asynchronous completion blocks, provided by user. All completion blocks are dispatched to the application's main thread.

## Examples

You can explore the sample app, provided [here](https://gitlab.com/DanKalinin.iOS.Applications/GSAuth.git). Common use cases are described below.

### Preparation

Client and Database instances should be accessible across application. A good place to store them is `Application` subclass. Custom singleton class also can be used for that.

```objectivec
@interface MyApplication : UIApplication

@property Gsa2Database *database;
@property Gsa2Client *client;

@end
```

Datasource file, required for Database, is included into framework's bundle and is available at `Gsa2DatabaseFile` path. Howewer, you need to put this file into location with write permissions enabled. User's Documents directory can be used for that.

```objectivec
NSURL *documents = [NSFileManager.defaultManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject;
NSURL *file = [documents URLByAppendingPathComponent:Gsa2DatabaseFile.lastPathComponent];
        
if (![NSFileManager.defaultManager fileExistsAtPath:file.path]) {
    (void)[NSFileManager.defaultManager copyItemAtPath:Gsa2DatabaseFile toPath:file.path error:nil];
}
```

Finally, initialize database and client properties.

```objectivec
self.database = [Gsa2Database databaseWithFile:file.path error:nil];
self.client = [Gsa2Client clientWithBase:@"https://smart-home-cloud.test.gs-labs.tv" database:self.database];
```

That's it! Now you can access the Database and Client instances from whatever `ViewController` you want.

### Login

Login means to obtain a Token for given Account, which is used for making future requests.

```objectivec
Gsa2Identity *identity = Gsa2Identity.new;
identity.type = @GSA2_IDENTITY_TYPE_PHONE;
identity.value = @"79999999955";

Gsa2Credential *credential = Gsa2Credential.new;
credential.type = @GSA2_CREDENTIAL_TYPE_PASSWORD;
credential.value = @"qwerty1234";

[MyApplication.sharedApplication.client tokenIssue:identity credential:credential completion:^(Gsa2Token *token, NSError *error) {
    if (token == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Success. Use token.access for protected requests.
    }
}];
```

At this point Token is cached in Database and can be retrieved for future requests as follows:

```objectivec
Gsa2Token *token = [MyApplication.sharedApplication.database tokenSelectBySubject:@GSA2_TOKEN_SUBJECT_ACCOUNT andId:accountId error:nil].firstObject;
```

### Account synchronization

This is the main synchronization request, which pulls down Account info, Domains and Controllers, belonging to the given Account.

```objectivec
[MyApplication.sharedApplication.client accountGet:token.access completion:^(Gsa2Account *account, NSError *error) {  
    if (account == nil) {
        NSLog(error.localizedDescription);
    } else {
        [self reload];
    }
}];
```

`reload` method can have the following imlementation:

```objectivec
self.domainsFull = [MyApplication.sharedApplication.database domainFullSelectByAccount:accountId tail:@"ORDER BY CAST(domain AS INTEGER) DESC" error:nil];
[self.tableView reloadData];
```

### Domain creation

Domain entity represents the home, owned by Account (Apartment, Cottage, etc). You can create one using the code below.

```objectivec
Gsa2Domain *domain = Gsa2Domain.new;
domain.name = @"My Home";

[MyApplication.sharedApplication.client domainCreate:domain controllersFull:nil access:token.access completion:^(Gsa2DomainFull *domainFull, NSError *error) {
    if (domainFull == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Home created
    }
}];
```

Then you can add Controllers and invite Guests to your Home.

Controllers are used to manage appliances, located at your Home.

Guests are other Accounts, which have access to your Home and can manage appliances located there.

### Guest invitation

```objectivec
Gsa2Invitation *invitation = Gsa2Invitation.new;
invitation.name = @"Welcome to My Home";
invitation.expiration = [NSDate dateWithTimeIntervalSinceNow:86400.0]; // Can be accepted in 24 hours
invitation.access = 604800.0; // Access for 7 days since accepted
        
[MyApplication.sharedApplication.client invitationCreate:invitation domainId:domainId access:token.access completion:^(Gsa2Invitation *invitation, NSError *error) {
    if (invitation == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Generate QR code, based on invitation.id and show it to the Guest
    }
}];
```

## Screenshots

Illustrate the [sample app](https://gitlab.com/DanKalinin.iOS.Applications/GSAuth.git) functionality.

| Login | Account | Homes | Owner | Guest |
| ------ | ------ | ------ | ------ | ------ |
| ![](images/login.png) | ![](images/account.png) | ![](images/homes.png) | ![](images/owner.png) | ![](images/guest.png) |

## TODO

- Compile binaries for x86_64 architecture (Simulator support).
- Recompile binaries with bitcode enabled.
