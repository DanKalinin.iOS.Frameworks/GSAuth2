//
//  Gsa2Database.m
//  GSAuth2
//
//  Created by Dan Kalinin on 8/23/20.
//

#import "Gsa2Database.h"

@implementation Gsa2DatabaseDelegates

@end

@implementation Gsa2Database

NSString *Gsa2DatabaseFile = nil;

void Gsa2DatabaseGSA2SQLEConnectionAccountDeleting(GSA2SQLEConnection *sdkSender, GSA2Account *sdkAccount, gpointer sdkSelf) {
    Gsa2Database *self = (__bridge Gsa2Database *)sdkSelf;
    Gsa2Account *account = [Gsa2Account from:sdkAccount];
    [self.delegates gsa2Database:self accountDeleting:account];
}

void Gsa2DatabaseGSA2SQLEConnectionAccountDomainDeleting(GSA2SQLEConnection *sdkSender, GSA2AccountDomain *sdkAccountDomain, gpointer sdkSelf) {
    Gsa2Database *self = (__bridge Gsa2Database *)sdkSelf;
    Gsa2AccountDomain *accountDomain = [Gsa2AccountDomain from:sdkAccountDomain];
    [self.delegates gsa2Database:self accountDomainDeleting:accountDomain];
}

void Gsa2DatabaseGSA2SQLEConnectionDomainControllerDeleting(GSA2SQLEConnection *sdkSender, GSA2DomainController *sdkDomainController, gpointer sdkSelf) {
    Gsa2Database *self = (__bridge Gsa2Database *)sdkSelf;
    Gsa2DomainController *domainController = [Gsa2DomainController from:sdkDomainController];
    [self.delegates gsa2Database:self domainControllerDeleting:domainController];
}

+ (void)load {
    NSBundle *bundle = [NSBundle bundleForClass:self];
    Gsa2DatabaseFile = [bundle pathForResource:@"gs-auth-2" ofType:@"sqlite3"];
}

- (void)dealloc {
    g_object_unref(self.object);
}

+ (instancetype)databaseWithFile:(NSString *)file error:(NSError **)error {
    Gsa2Database *ret = nil;
    g_autoptr(GError) sdkError = NULL;
    GSA2SQLEConnection *object = gsa2_sqle_connection_new((gchar *)file.UTF8String, &sdkError);
    
    if (object == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = self.new;
        ret.object = object;
        ret.file = file;
        ret.delegates = [Gsa2DatabaseDelegates.alloc initWithStorage:NSPointerArray.weakObjectsPointerArray];
        gpointer sdkRet = (__bridge gpointer)ret;
        (void)g_signal_connect(object, "account-deleting", G_CALLBACK(Gsa2DatabaseGSA2SQLEConnectionAccountDeleting), sdkRet);
        (void)g_signal_connect(object, "account-domain-deleting", G_CALLBACK(Gsa2DatabaseGSA2SQLEConnectionAccountDomainDeleting), sdkRet);
        (void)g_signal_connect(object, "domain-controller-deleting", G_CALLBACK(Gsa2DatabaseGSA2SQLEConnectionDomainControllerDeleting), sdkRet);
    }
    
    return ret;
}

- (NSArray<NSNumber *> *)selectInt:(NSString *)tail error:(NSError **)error {
    NSArray<NSNumber *> *ret = nil;
    g_autoptr(GList) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (sqle_connection_select_int(SQLE_CONNECTION_OBJECT(self.object), (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gListGintFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (NSArray<NSString *> *)selectText:(NSString *)tail error:(NSError **)error {
    NSArray<NSString *> *ret = nil;
    g_autolist(gchar) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (sqle_connection_select_text(SQLE_CONNECTION_OBJECT(self.object), (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gListGchararrayFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - Account

- (BOOL)accountDelete:(NSString *)tail error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_account_delete(self.object, (gchar *)tail.UTF8String, &sdkError) == SQLITE_DONE) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (NSArray<Gsa2Account *> *)accountSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2Account *> *ret = nil;
    g_autolist(GSA2Account) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_account_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2AccountFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - Domain

- (NSArray<Gsa2Domain *> *)domainSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2Domain *> *ret = nil;
    g_autolist(GSA2Domain) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_domain_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2DomainFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - Controller

- (NSArray<Gsa2Controller *> *)controllerSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2Controller *> *ret = nil;
    g_autolist(GSA2Controller) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_controller_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2ControllerFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - AccountDomain

- (NSArray<Gsa2AccountDomain *> *)accountDomainSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2AccountDomain *> *ret = nil;
    g_autolist(GSA2AccountDomain) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_account_domain_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2AccountDomainFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - DomainController

- (NSArray<Gsa2DomainController *> *)domainControllerSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2DomainController *> *ret = nil;
    g_autolist(GSA2DomainController) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_domain_controller_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2DomainControllerFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - Identity

- (NSArray<Gsa2Identity *> *)identitySelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2Identity *> *ret = nil;
    g_autolist(GSA2Identity) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_identity_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2IdentityFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - Invitation

- (NSArray<Gsa2Invitation *> *)invitationSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2Invitation *> *ret = nil;
    g_autolist(GSA2Invitation) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_invitation_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2InvitationFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - Token

- (NSArray<Gsa2Token *> *)tokenSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2Token *> *ret = nil;
    g_autolist(GSA2Token) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_token_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2TokenFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - AccountFull

- (NSArray<Gsa2AccountFull *> *)accountFullSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2AccountFull *> *ret = nil;
    g_autolist(GSA2AccountFull) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_account_full_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2AccountFullFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - DomainFull

- (NSArray<Gsa2DomainFull *> *)domainFullSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2DomainFull *> *ret = nil;
    g_autolist(GSA2DomainFull) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_sqle_connection_domain_full_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2DomainFullFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

#pragma mark - ControllerFull

- (NSArray<Gsa2ControllerFull *> *)controllerFullSelect:(NSString *)tail error:(NSError **)error {
    NSArray<Gsa2ControllerFull *> *ret = nil;
    g_autolist(GSA2ControllerFull) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;

    if (gsa2_sqle_connection_controller_full_select(self.object, (gchar *)tail.UTF8String, &sdkRet, &sdkError) == SQLITE_DONE) {
        ret = [NSArray gsa2ControllerFullFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }

    return ret;
}

@end
