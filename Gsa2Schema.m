//
//  Gsa2Schema.m
//  GSAuth2
//
//  Created by Dan Kalinin on 8/23/20.
//

#import "Gsa2Schema.h"

#pragma mark - Account

@implementation Gsa2Account

+ (instancetype)from:(GSA2Account *)sdkAccount {
    Gsa2Account *ret = self.new;
    ret.id = NSE_BOX(sdkAccount->id);
    ret.name = NSE_BOX(sdkAccount->name);
    ret.password = NSE_BOX(sdkAccount->password);
    return ret;
}

- (GSA2Account *)to {
    GSA2Account *ret = g_new0(GSA2Account, 1);
    gsa2_account_set_id(ret, (gchar *)self.id.UTF8String);
    gsa2_account_set_name(ret, (gchar *)self.name.UTF8String);
    gsa2_account_set_password(ret, (gchar *)self.password.UTF8String);
    return ret;
}

@end

@implementation NSArray (Gsa2Account)

+ (instancetype)gsa2AccountFrom:(GList *)sdkAccounts {
    NSMutableArray<Gsa2Account *> *ret = NSMutableArray.array;
    
    for (GList *sdkAccount = sdkAccounts; sdkAccount != NULL; sdkAccount = sdkAccount->next) {
        Gsa2Account *account = [Gsa2Account from:sdkAccount->data];
        [ret addObject:account];
    }
    
    return ret;
}

@end

#pragma mark - Domain

@implementation Gsa2Domain

+ (instancetype)from:(GSA2Domain *)sdkDomain {
    Gsa2Domain *ret = self.new;
    ret.id = NSE_BOX(sdkDomain->id);
    ret.owner = NSE_BOX(sdkDomain->owner);
    ret.name = NSE_BOX(sdkDomain->name);
    return ret;
}

- (GSA2Domain *)to {
    GSA2Domain *ret = g_new0(GSA2Domain, 1);
    gsa2_domain_set_id(ret, (gchar *)self.id.UTF8String);
    gsa2_domain_set_owner(ret, (gchar *)self.owner.UTF8String);
    gsa2_domain_set_name(ret, (gchar *)self.name.UTF8String);
    return ret;
}

@end

@implementation NSArray (Gsa2Domain)

+ (instancetype)gsa2DomainFrom:(GList *)sdkDomains {
    NSMutableArray<Gsa2Domain *> *ret = NSMutableArray.array;
    
    for (GList *sdkDomain = sdkDomains; sdkDomain != NULL; sdkDomain = sdkDomain->next) {
        Gsa2Domain *domain = [Gsa2Domain from:sdkDomain->data];
        [ret addObject:domain];
    }
    
    return ret;
}

@end

#pragma mark - Controller

@implementation Gsa2Controller

+ (instancetype)from:(GSA2Controller *)sdkController {
    Gsa2Controller *ret = self.new;
    ret.id = NSE_BOX(sdkController->id);
    ret.mac = NSE_BOX(sdkController->mac);
    ret.model = NSE_BOX(sdkController->model);
    ret.serial = NSE_BOX(sdkController->serial);
    return ret;
}

- (GSA2Controller *)to {
    GSA2Controller *ret = g_new0(GSA2Controller, 1);
    gsa2_controller_set_id(ret, (gchar *)self.id.UTF8String);
    gsa2_controller_set_mac(ret, (gchar *)self.mac.UTF8String);
    gsa2_controller_set_model(ret, (gchar *)self.model.UTF8String);
    gsa2_controller_set_serial(ret, (gchar *)self.serial.UTF8String);
    return ret;
}

@end

@implementation NSArray (Gsa2Controller)

+ (instancetype)gsa2ControllerFrom:(GList *)sdkControllers {
    NSMutableArray<Gsa2Controller *> *ret = NSMutableArray.array;
    
    for (GList *sdkController = sdkControllers; sdkController != NULL; sdkController = sdkController->next) {
        Gsa2Controller *controller = [Gsa2Controller from:sdkController->data];
        [ret addObject:controller];
    }
    
    return ret;
}

@end

#pragma mark - AccountDomain

@implementation Gsa2AccountDomain

+ (instancetype)from:(GSA2AccountDomain *)sdkAccountDomain {
    Gsa2AccountDomain *ret = self.new;
    ret.account = NSE_BOX(sdkAccountDomain->account);
    ret.domain = NSE_BOX(sdkAccountDomain->domain);
    ret.expiration = NSE_BOX(sdkAccountDomain->expiration);
    ret.current = sdkAccountDomain->current;
    return ret;
}

- (void)setNsDateExpiration:(NSDate *)nsDateExpiration {
    NSISO8601DateFormatter *formatter = NSISO8601DateFormatter.new;
    self.expiration = [formatter stringFromDate:nsDateExpiration];
}

- (NSDate *)nsDateExpiration {
    NSISO8601DateFormatter *formatter = NSISO8601DateFormatter.new;
    NSDate *ret = [formatter dateFromString:self.expiration];
    return ret;
}

@end

@implementation NSArray (Gsa2AccountDomain)

+ (instancetype)gsa2AccountDomainFrom:(GList *)sdkAccountDomains {
    NSMutableArray<Gsa2AccountDomain *> *ret = NSMutableArray.array;
    
    for (GList *sdkAccountDomain = sdkAccountDomains; sdkAccountDomain != NULL; sdkAccountDomain = sdkAccountDomain->next) {
        Gsa2AccountDomain *accountDomain = [Gsa2AccountDomain from:sdkAccountDomain->data];
        [ret addObject:accountDomain];
    }
    
    return ret;
}

@end

#pragma mark - DomainController

@implementation Gsa2DomainController

+ (instancetype)from:(GSA2DomainController *)sdkDomainController {
    Gsa2DomainController *ret = self.new;
    ret.domain = NSE_BOX(sdkDomainController->domain);
    ret.controller = NSE_BOX(sdkDomainController->controller);
    ret.key = NSE_BOX(sdkDomainController->key);
    ret.ip = NSE_BOX(sdkDomainController->ip);
    ret.port = sdkDomainController->port;
    ret.bssid = NSE_BOX(sdkDomainController->bssid);
    ret.verified = sdkDomainController->verified;
    return ret;
}

- (GSA2DomainController *)to {
    GSA2DomainController *ret = g_new0(GSA2DomainController, 1);
    gsa2_domain_controller_set_domain(ret, (gchar *)self.domain.UTF8String);
    gsa2_domain_controller_set_controller(ret, (gchar *)self.controller.UTF8String);
    gsa2_domain_controller_set_key(ret, (gchar *)self.key.UTF8String);
    gsa2_domain_controller_set_ip(ret, (gchar *)self.ip.UTF8String);
    ret->port = (gint)self.port;
    gsa2_domain_controller_set_bssid(ret, (gchar *)self.bssid.UTF8String);
    ret->verified = (gint)self.verified;
    return ret;
}

@end

@implementation NSArray (Gsa2DomainController)

+ (instancetype)gsa2DomainControllerFrom:(GList *)sdkDomainControllers {
    NSMutableArray<Gsa2DomainController *> *ret = NSMutableArray.array;
    
    for (GList *sdkDomainController = sdkDomainControllers; sdkDomainController != NULL; sdkDomainController = sdkDomainController->next) {
        Gsa2DomainController *domainController = [Gsa2DomainController from:sdkDomainController->data];
        [ret addObject:domainController];
    }
    
    return ret;
}

@end

#pragma mark - Identity

@implementation Gsa2Identity

+ (instancetype)from:(GSA2Identity *)sdkIdentity {
    Gsa2Identity *ret = self.new;
    ret.type = NSE_BOX(sdkIdentity->type);
    ret.value = NSE_BOX(sdkIdentity->value);
    ret.account = NSE_BOX(sdkIdentity->account);
    ret.controller = NSE_BOX(sdkIdentity->controller);
    return ret;
}

- (GSA2Identity *)to {
    GSA2Identity *ret = g_new0(GSA2Identity, 1);
    gsa2_identity_set_type(ret, (gchar *)self.type.UTF8String);
    gsa2_identity_set_value(ret, (gchar *)self.value.UTF8String);
    gsa2_identity_set_account(ret, (gchar *)self.account.UTF8String);
    gsa2_identity_set_controller(ret, (gchar *)self.controller.UTF8String);
    return ret;
}

@end

@implementation NSArray (Gsa2Identity)

+ (instancetype)gsa2IdentityFrom:(GList *)sdkIdentities {
    NSMutableArray<Gsa2Identity *> *ret = NSMutableArray.array;
    
    for (GList *sdkIdentity = sdkIdentities; sdkIdentity != NULL; sdkIdentity = sdkIdentity->next) {
        Gsa2Identity *identity = [Gsa2Identity from:sdkIdentity->data];
        [ret addObject:identity];
    }
    
    return ret;
}

- (GList *)gsa2IdentityTo {
    GList *ret = NULL;
    
    for (Gsa2Identity *identity in self) {
        ret = g_list_append(ret, identity.to);
    }
    
    return ret;
}

@end

#pragma mark - Invitation

@implementation Gsa2Invitation

+ (instancetype)from:(GSA2Invitation *)sdkInvitation {
    Gsa2Invitation *ret = self.new;
    ret.id = NSE_BOX(sdkInvitation->id);
    ret.domain = NSE_BOX(sdkInvitation->domain);
    ret.name = NSE_BOX(sdkInvitation->name);
    ret.expiration = NSE_BOX(sdkInvitation->expiration);
    ret.access = sdkInvitation->access;
    return ret;
}

- (GSA2Invitation *)to {
    GSA2Invitation *ret = g_new0(GSA2Invitation, 1);
    gsa2_invitation_set_id(ret, (gchar *)self.id.UTF8String);
    gsa2_invitation_set_domain(ret, (gchar *)self.domain.UTF8String);
    gsa2_invitation_set_name(ret, (gchar *)self.name.UTF8String);
    gsa2_invitation_set_expiration(ret, (gchar *)self.expiration.UTF8String);
    ret->access = self.access;
    return ret;
}

- (void)setNsDateExpiration:(NSDate *)nsDateExpiration {
    self.expiration = [NSString stringWithFormat:@"%.0f", self.nsDateExpiration.timeIntervalSinceNow];
}

- (NSDate *)nsDateExpiration {
    NSISO8601DateFormatter *formatter = NSISO8601DateFormatter.new;
    NSDate *ret = [formatter dateFromString:self.expiration];
    return ret;
}

@end

@implementation NSArray (Gsa2Invitation)

+ (instancetype)gsa2InvitationFrom:(GList *)sdkInvitations {
    NSMutableArray<Gsa2Invitation *> *ret = NSMutableArray.array;

    for (GList *sdkInvitation = sdkInvitations; sdkInvitation != NULL; sdkInvitation = sdkInvitation->next) {
        Gsa2Invitation *invitation = [Gsa2Invitation from:sdkInvitation->data];
        [ret addObject:invitation];
    }

    return ret;
}

@end

#pragma mark - Token

@implementation Gsa2Token

+ (instancetype)from:(GSA2Token *)sdkToken {
    Gsa2Token *ret = self.new;
    ret.subject = NSE_BOX(sdkToken->subject);
    ret.id = NSE_BOX(sdkToken->id);
    ret.access = NSE_BOX(sdkToken->access);
    ret.refresh = NSE_BOX(sdkToken->refresh);
    return ret;
}

@end

@implementation NSArray (Gsa2Token)

+ (instancetype)gsa2TokenFrom:(GList *)sdkTokens {
    NSMutableArray<Gsa2Token *> *ret = NSMutableArray.array;
    
    for (GList *sdkToken = sdkTokens; sdkToken != NULL; sdkToken = sdkToken->next) {
        Gsa2Token *token = [Gsa2Token from:sdkToken->data];
        [ret addObject:token];
    }
    
    return ret;
}

@end

#pragma mark - Credential

@implementation Gsa2Credential

- (GSA2Credential *)to {
    GSA2Credential *ret = g_new0(GSA2Credential, 1);
    gsa2_credential_set_type(ret, (gchar *)self.type.UTF8String);
    gsa2_credential_set_value(ret, (gchar *)self.value.UTF8String);
    return ret;
}

@end

#pragma mark - AccountFull

@implementation Gsa2AccountFull

+ (instancetype)from:(GSA2AccountFull *)sdkAccountFull {
    Gsa2AccountFull *ret = self.new;
    ret.account = [Gsa2Account from:sdkAccountFull->account];
    ret.accountDomain = [Gsa2AccountDomain from:sdkAccountFull->account_domain];
    return ret;
}

@end

@implementation NSArray (Gsa2AccountFull)

+ (instancetype)gsa2AccountFullFrom:(GList *)sdkAccountsFull {
    NSMutableArray<Gsa2AccountFull *> *ret = NSMutableArray.array;
    
    for (GList *sdkAccountFull = sdkAccountsFull; sdkAccountFull != NULL; sdkAccountFull = sdkAccountFull->next) {
        Gsa2AccountFull *accountFull = [Gsa2AccountFull from:sdkAccountFull->data];
        [ret addObject:accountFull];
    }
    
    return ret;
}

@end

#pragma mark - DomainFull

@implementation Gsa2DomainFull

+ (instancetype)from:(GSA2DomainFull *)sdkDomainFull {
    Gsa2DomainFull *ret = self.new;
    ret.domain = [Gsa2Domain from:sdkDomainFull->domain];
    ret.accountDomain = [Gsa2AccountDomain from:sdkDomainFull->account_domain];
    return ret;
}

@end

@implementation NSArray (Gsa2DomainFull)

+ (instancetype)gsa2DomainFullFrom:(GList *)sdkDomainsFull {
    NSMutableArray<Gsa2DomainFull *> *ret = NSMutableArray.array;

    for (GList *sdkDomainFull = sdkDomainsFull; sdkDomainFull != NULL; sdkDomainFull = sdkDomainFull->next) {
        Gsa2DomainFull *domainFull = [Gsa2DomainFull from:sdkDomainFull->data];
        [ret addObject:domainFull];
    }

    return ret;
}

@end

#pragma mark - ControllerFull

@implementation Gsa2ControllerFull

+ (instancetype)from:(GSA2ControllerFull *)sdkControllerFull {
    Gsa2ControllerFull *ret = self.new;
    ret.controller = [Gsa2Controller from:sdkControllerFull->controller];
    ret.domainController = [Gsa2DomainController from:sdkControllerFull->domain_controller];
    return ret;
}

- (GSA2ControllerFull *)to {
    GSA2ControllerFull *ret = g_new0(GSA2ControllerFull, 1);
    ret->controller = self.controller.to;
    ret->domain_controller = self.domainController.to;
    return ret;
}

@end

@implementation NSArray (Gsa2ControllerFull)

+ (instancetype)gsa2ControllerFullFrom:(GList *)sdkControllersFull {
    NSMutableArray<Gsa2ControllerFull *> *ret = NSMutableArray.array;

    for (GList *sdkControllerFull = sdkControllersFull; sdkControllerFull != NULL; sdkControllerFull = sdkControllerFull->next) {
        Gsa2ControllerFull *controllerFull = [Gsa2ControllerFull from:sdkControllerFull->data];
        [ret addObject:controllerFull];
    }

    return ret;
}

- (GList *)gsa2ControllerFullTo {
    GList *ret = NULL;

    for (Gsa2ControllerFull *controllerFull in self) {
        ret = g_list_append(ret, controllerFull.to);
    }

    return ret;
}

@end
