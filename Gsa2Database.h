//
//  Gsa2Database.h
//  GSAuth2
//
//  Created by Dan Kalinin on 8/23/20.
//

#import "Gsa2Main.h"
#import "Gsa2Schema.h"

@class Gsa2Database;

@protocol Gsa2DatabaseDelegate <NSObject>

@optional
- (void)gsa2Database:(Gsa2Database *)sender accountDeleting:(Gsa2Account *)account;
- (void)gsa2Database:(Gsa2Database *)sender accountDomainDeleting:(Gsa2AccountDomain *)accountDomain;
- (void)gsa2Database:(Gsa2Database *)sender domainControllerDeleting:(Gsa2DomainController *)domainController;

@end

@interface Gsa2DatabaseDelegates : NseArray <Gsa2DatabaseDelegate>

@end

@interface Gsa2Database : NseObject

extern NSString *Gsa2DatabaseFile;

@property GSA2SQLEConnection *object;
@property NSString *file;
@property Gsa2DatabaseDelegates *delegates;

+ (instancetype)databaseWithFile:(NSString *)file error:(NSError **)error;

- (NSArray<NSNumber *> *)selectInt:(NSString *)tail error:(NSError **)error;
- (NSArray<NSString *> *)selectText:(NSString *)tail error:(NSError **)error;

#pragma mark - Account

- (BOOL)accountDelete:(NSString *)tail error:(NSError **)error;
- (NSArray<Gsa2Account *> *)accountSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - Domain

- (NSArray<Gsa2Domain *> *)domainSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - Controller

- (NSArray<Gsa2Controller *> *)controllerSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - AccountDomain

- (NSArray<Gsa2AccountDomain *> *)accountDomainSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - DomainController

- (NSArray<Gsa2DomainController *> *)domainControllerSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - Identity

- (NSArray<Gsa2Identity *> *)identitySelect:(NSString *)tail error:(NSError **)error;

#pragma mark - Invitation

- (NSArray<Gsa2Invitation *> *)invitationSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - Token

- (NSArray<Gsa2Token *> *)tokenSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - AccountFull

- (NSArray<Gsa2AccountFull *> *)accountFullSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - DomainFull

- (NSArray<Gsa2DomainFull *> *)domainFullSelect:(NSString *)tail error:(NSError **)error;

#pragma mark - ControllerFull

- (NSArray<Gsa2ControllerFull *> *)controllerFullSelect:(NSString *)tail error:(NSError **)error;

@end
