//
//  Gsa2Client.m
//  GSAuth2
//
//  Created by Dan Kalinin on 8/23/20.
//

#import "Gsa2Client.h"

@implementation Gsa2Client

- (void)dealloc {
    g_object_unref(self.object);
}

+ (instancetype)clientWithBase:(NSString *)base database:(Gsa2Database *)database {
    Gsa2Client *ret = self.new;
    g_autoptr(SoupURI) sdkBase = soup_uri_new(base.UTF8String);
    ret.object = gsa2_soe_session_new(sdkBase, database.object);
    ret.base = base;
    ret.database = database;
    return ret;
}

- (void)abort {
    soup_session_abort(SOUP_SESSION(self.object));
}

#pragma mark - IdentityCheck

- (BOOL)identityCheckSync:(Gsa2Identity *)identity available:(BOOL *)available error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GSA2Identity) sdkIdentity = identity.to;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_identity_check_sync(self.object, sdkIdentity, (gboolean *)available, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)identityCheckAsync:(Gsa2Identity *)identity completion:(Gsa2ClientIdentityCheckAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        BOOL available = NO;
        NSError *error = nil;
        (void)[self identityCheckSync:identity available:&available error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, available, error);
        }];
    });
}

#pragma mark - CodeSend

- (NSDate *)codeSendSync:(Gsa2Identity *)identity error:(NSError **)error {
    NSDate *ret = nil;
    g_autoptr(GDateTime) sdkRet = NULL;
    g_autoptr(GSA2Identity) sdkIdentity = identity.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_code_send_sync(self.object, sdkIdentity, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [NSDate gDateTimeFrom:sdkRet];
    }
    
    return ret;
}

- (void)codeSendAsync:(Gsa2Identity *)identity completion:(Gsa2ClientCodeSendAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSDate *expiration = [self codeSendSync:identity error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, expiration, error);
        }];
    });
}

#pragma mark - AccountCreate

- (Gsa2Account *)accountCreateSync:(Gsa2Account *)account identities:(NSArray<Gsa2Identity *> *)identities expiration:(NSDate **)expiration error:(NSError **)error {
    Gsa2Account *ret = nil;
    g_autoptr(GSA2Account) sdkRet = NULL;
    g_autoptr(GSA2Account) sdkAccount = account.to;
    g_autolist(GSA2Identity) sdkIdentities = identities.gsa2IdentityTo;
    g_autoptr(GDateTime) sdkExpiration = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_account_create_sync(self.object, sdkAccount, sdkIdentities, &sdkExpiration, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2Account from:sdkRet];
        GE_SET_VALUE(expiration, [NSDate gDateTimeFrom:sdkExpiration]);
    }
    
    return ret;
}

- (void)accountCreateAsync:(Gsa2Account *)account identities:(NSArray<Gsa2Identity *> *)identities completion:(Gsa2ClientAccountCreateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSDate *expiration = nil;
        NSError *error = nil;
        Gsa2Account *outAccount = [self accountCreateSync:account identities:identities expiration:&expiration error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outAccount, expiration, error);
        }];
    });
}

#pragma mark - AccountGet

- (Gsa2Account *)accountGetSync:(NSString *)access error:(NSError **)error {
    Gsa2Account *ret = nil;
    g_autoptr(GSA2Account) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_account_get_sync(self.object, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2Account from:sdkRet];
    }
    
    return ret;
}

- (void)accountGetAsync:(NSString *)access completion:(Gsa2ClientAccountGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        Gsa2Account *outAccount = [self accountGetSync:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outAccount, error);
        }];
    });
}

#pragma mark - PasswordUpdate

- (BOOL)passwordUpdateSync:(NSString *)password access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_password_update_sync(self.object, (gchar *)password.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)passwordUpdateAsync:(NSString *)password access:(NSString *)access completion:(Gsa2ClientPasswordUpdateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self passwordUpdateSync:password access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - DomainCreate

- (Gsa2DomainFull *)domainCreateSync:(Gsa2Domain *)domain controllersFull:(NSArray<Gsa2ControllerFull *> *)controllersFull access:(NSString *)access error:(NSError **)error {
    Gsa2DomainFull *ret = nil;
    g_autoptr(GSA2DomainFull) sdkRet = NULL;
    g_autoptr(GSA2Domain) sdkDomain = domain.to;
    g_autolist(GSA2ControllerFull) sdkControllersFull = controllersFull.gsa2ControllerFullTo;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_domain_create_sync(self.object, sdkDomain, sdkControllersFull, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2DomainFull from:sdkRet];
    }
    
    return ret;
}

- (void)domainCreateAsync:(Gsa2Domain *)domain controllersFull:(NSArray<Gsa2ControllerFull *> *)controllersFull access:(NSString *)access completion:(Gsa2ClientDomainCreateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        Gsa2DomainFull *outDomainFull = [self domainCreateSync:domain controllersFull:controllersFull access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outDomainFull, error);
        }];
    });
}

#pragma mark - DomainRename

- (BOOL)domainRenameSync:(NSString *)domainId name:(NSString *)name access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_domain_rename_sync(self.object, (gchar *)domainId.UTF8String, (gchar *)name.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)domainRenameAsync:(NSString *)domainId name:(NSString *)name access:(NSString *)access completion:(Gsa2ClientDomainRenameAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self domainRenameSync:domainId name:name access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - DomainDelete

- (BOOL)domainDeleteSync:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_domain_delete_sync(self.object, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)domainDeleteAsync:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientDomainDeleteAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self domainDeleteSync:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - DomainExit

- (BOOL)domainExitSync:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_domain_exit_sync(self.object, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)domainExitAsync:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientDomainExitAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self domainExitSync:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - GuestDelete

- (Gsa2DomainFull *)guestDeleteSync:(NSString *)guestId domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    Gsa2DomainFull *ret = nil;
    g_autoptr(GSA2DomainFull) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_guest_delete_sync(self.object, (gchar *)guestId.UTF8String, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2DomainFull from:sdkRet];
    }
    
    return ret;
}

- (void)guestDeleteAsync:(NSString *)guestId domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientGuestDeleteAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        Gsa2DomainFull *outDomainFull = [self guestDeleteSync:guestId domainId:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outDomainFull, error);
        }];
    });
}

#pragma mark - InvitationCreate

- (Gsa2Invitation *)invitationCreateSync:(Gsa2Invitation *)invitation domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    Gsa2Invitation *ret = nil;
    g_autoptr(GSA2Invitation) sdkRet = NULL;
    g_autoptr(GSA2Invitation) sdkInvitation = invitation.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_invitation_create_sync(self.object, sdkInvitation, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2Invitation from:sdkRet];
    }
    
    return ret;
}

- (void)invitationCreateAsync:(Gsa2Invitation *)invitation domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientInvitationCreateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        Gsa2Invitation *outInvitation = [self invitationCreateSync:invitation domainId:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outInvitation, error);
        }];
    });
}

#pragma mark - InvitationRevoke

- (BOOL)invitationRevokeSync:(NSString *)invitationId domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_invitation_revoke_sync(self.object, (gchar *)invitationId.UTF8String, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)invitationRevokeAsync:(NSString *)invitationId domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientInvitationRevokeAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self invitationRevokeSync:invitationId domainId:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - InvitationAccept

- (Gsa2DomainFull *)invitationAcceptSync:(NSString *)invitationId access:(NSString *)access error:(NSError **)error {
    Gsa2DomainFull *ret = nil;
    g_autoptr(GSA2DomainFull) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_invitation_accept_sync(self.object, (gchar *)invitationId.UTF8String, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2DomainFull from:sdkRet];
    }
    
    return ret;
}

- (void)invitationAcceptAsync:(NSString *)invitationId access:(NSString *)access completion:(Gsa2ClientInvitationAcceptAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        Gsa2DomainFull *outDomainFull = [self invitationAcceptSync:invitationId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outDomainFull, error);
        }];
    });
}

#pragma mark - CodeGet

- (NSString *)codeGetSync:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    NSString *ret = nil;
    g_autofree gchar *sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_code_get_sync(self.object, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = NSE_BOX(sdkRet);
    }
    
    return ret;
}

- (void)codeGetAsync:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientCodeGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSString *code = [self codeGetSync:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, code, error);
        }];
    });
}

#pragma mark - ControllersAdd

- (NSArray<Gsa2ControllerFull *> *)controllersAddSync:(NSArray<Gsa2ControllerFull *> *)controllersFull domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    NSArray<Gsa2ControllerFull *> *ret = nil;
    g_autolist(GSA2ControllerFull) sdkRet = NULL;
    g_autolist(GSA2ControllerFull) sdkControllersFull = controllersFull.gsa2ControllerFullTo;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_controllers_add_sync(self.object, sdkControllersFull, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkRet, &sdkError)) {
        ret = [NSArray gsa2ControllerFullFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)controllersAddAsync:(NSArray<Gsa2ControllerFull *> *)controllersFull domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientControllersAddAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSArray<Gsa2ControllerFull *> *outControllersFull = [self controllersAddSync:controllersFull domainId:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outControllersFull, error);
        }];
    });
}

#pragma mark - ControllerRemove

- (BOOL)controllerRemoveSync:(NSString *)controllerId domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_controller_remove_sync(self.object, (gchar *)controllerId.UTF8String, (gchar *)domainId.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)controllerRemoveAsync:(NSString *)controllerId domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientControllerRemoveAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self controllerRemoveSync:controllerId domainId:domainId access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - TokenIssue

- (Gsa2Token *)tokenIssueSync:(Gsa2Identity *)identity credential:(Gsa2Credential *)credential error:(NSError **)error {
    Gsa2Token *ret = nil;
    g_autoptr(GSA2Token) sdkRet = NULL;
    g_autoptr(GSA2Identity) sdkIdentity = identity.to;
    g_autoptr(GSA2Credential) sdkCredential = credential.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = gsa2_soe_session_token_issue_sync(self.object, sdkIdentity, sdkCredential, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [Gsa2Token from:sdkRet];
    }
    
    return ret;
}

- (void)tokenIssueAsync:(Gsa2Identity *)identity credential:(Gsa2Credential *)credential completion:(Gsa2ClientTokenIssueAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        Gsa2Token *outToken = [self tokenIssueSync:identity credential:credential error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, outToken, error);
        }];
    });
}

#pragma mark - TokenRevoke

- (BOOL)tokenRevokeSync:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_token_revoke_sync(self.object, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)tokenRevokeAsync:(NSString *)access completion:(Gsa2ClientTokenRevokeAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self tokenRevokeSync:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - CameraPlaylist

- (NSString *)cameraPlaylistSync:(NSString *)id key:(NSString *)key controller:(NSString *)controller access:(NSString *)access error:(NSError **)error {
    NSString *ret = nil;
    g_autofree gchar *sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = gsa2_soe_session_camera_playlist_sync(self.object, (gchar *)id.UTF8String, (gchar *)key.UTF8String, (gchar *)controller.UTF8String, (gchar *)access.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = NSE_BOX(sdkRet);
    }

    return ret;
}

- (void)cameraPlaylistAsync:(NSString *)id key:(NSString *)key controller:(NSString *)controller access:(NSString *)access completion:(Gsa2ClientCameraPlaylistAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSString *playlist = [self cameraPlaylistSync:id key:key controller:controller access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, playlist, error);
        }];
    });
}

#pragma mark - CameraHeartbeat

- (BOOL)cameraHeartbeatSync:(NSString *)id controller:(NSString *)controller access:(NSString *)access error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (gsa2_soe_session_camera_heartbeat_sync(self.object, (gchar *)id.UTF8String, (gchar *)controller.UTF8String, (gchar *)access.UTF8String, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)cameraHeartbeatAsync:(NSString *)id controller:(NSString *)controller access:(NSString *)access completion:(Gsa2ClientCameraHeartbeatAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self cameraHeartbeatSync:id controller:controller access:access error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

@end
