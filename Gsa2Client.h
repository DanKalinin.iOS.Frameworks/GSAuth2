//
//  Gsa2Client.h
//  GSAuth2
//
//  Created by Dan Kalinin on 8/23/20.
//

#import "Gsa2Main.h"
#import "Gsa2Schema.h"
#import "Gsa2Database.h"

@interface Gsa2Client : NseObject

@property GSA2SOESession *object;
@property NSString *base;
@property Gsa2Database *database;

+ (instancetype)clientWithBase:(NSString *)base database:(Gsa2Database *)database;

- (void)abort;

#pragma mark - IdentityCheck

- (BOOL)identityCheckSync:(Gsa2Identity *)identity available:(BOOL *)available error:(NSError **)error;

typedef void (^Gsa2ClientIdentityCheckAsyncCompletion)(BOOL available, NSError *error);
- (void)identityCheckAsync:(Gsa2Identity *)identity completion:(Gsa2ClientIdentityCheckAsyncCompletion)completion;

#pragma mark - CodeSend

- (NSDate *)codeSendSync:(Gsa2Identity *)identity error:(NSError **)error;

typedef void (^Gsa2ClientCodeSendAsyncCompletion)(NSDate *expiration, NSError *error);
- (void)codeSendAsync:(Gsa2Identity *)identity completion:(Gsa2ClientCodeSendAsyncCompletion)completion;

#pragma mark - AccountCreate

- (Gsa2Account *)accountCreateSync:(Gsa2Account *)account identities:(NSArray<Gsa2Identity *> *)identities expiration:(NSDate **)expiration error:(NSError **)error;

typedef void (^Gsa2ClientAccountCreateAsyncCompletion)(Gsa2Account *account, NSDate *expiration, NSError *error);
- (void)accountCreateAsync:(Gsa2Account *)account identities:(NSArray<Gsa2Identity *> *)identities completion:(Gsa2ClientAccountCreateAsyncCompletion)completion;

#pragma mark - AccountGet

- (Gsa2Account *)accountGetSync:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientAccountGetAsyncCompletion)(Gsa2Account *account, NSError *error);
- (void)accountGetAsync:(NSString *)access completion:(Gsa2ClientAccountGetAsyncCompletion)completion;

#pragma mark - PasswordUpdate

- (BOOL)passwordUpdateSync:(NSString *)password access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientPasswordUpdateAsyncCompletion)(NSError *error);
- (void)passwordUpdateAsync:(NSString *)password access:(NSString *)access completion:(Gsa2ClientPasswordUpdateAsyncCompletion)completion;

#pragma mark - DomainCreate

- (Gsa2DomainFull *)domainCreateSync:(Gsa2Domain *)domain controllersFull:(NSArray<Gsa2ControllerFull *> *)controllersFull access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientDomainCreateAsyncCompletion)(Gsa2DomainFull *domainFull, NSError *error);
- (void)domainCreateAsync:(Gsa2Domain *)domain controllersFull:(NSArray<Gsa2ControllerFull *> *)controllersFull access:(NSString *)access completion:(Gsa2ClientDomainCreateAsyncCompletion)completion;

#pragma mark - DomainRename

- (BOOL)domainRenameSync:(NSString *)domainId name:(NSString *)name access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientDomainRenameAsyncCompletion)(NSError *error);
- (void)domainRenameAsync:(NSString *)domainId name:(NSString *)name access:(NSString *)access completion:(Gsa2ClientDomainRenameAsyncCompletion)completion;

#pragma mark - DomainDelete

- (BOOL)domainDeleteSync:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientDomainDeleteAsyncCompletion)(NSError *error);
- (void)domainDeleteAsync:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientDomainDeleteAsyncCompletion)completion;

#pragma mark - DomainExit

- (BOOL)domainExitSync:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientDomainExitAsyncCompletion)(NSError *error);
- (void)domainExitAsync:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientDomainExitAsyncCompletion)completion;

#pragma mark - GuestDelete

- (Gsa2DomainFull *)guestDeleteSync:(NSString *)guestId domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientGuestDeleteAsyncCompletion)(Gsa2DomainFull *domainFull, NSError *error);
- (void)guestDeleteAsync:(NSString *)guestId domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientGuestDeleteAsyncCompletion)completion;

#pragma mark - InvitationCreate

- (Gsa2Invitation *)invitationCreateSync:(Gsa2Invitation *)invitation domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientInvitationCreateAsyncCompletion)(Gsa2Invitation *invitation, NSError *error);
- (void)invitationCreateAsync:(Gsa2Invitation *)invitation domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientInvitationCreateAsyncCompletion)completion;

#pragma mark - InvitationRevoke

- (BOOL)invitationRevokeSync:(NSString *)invitationId domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientInvitationRevokeAsyncCompletion)(NSError *error);
- (void)invitationRevokeAsync:(NSString *)invitationId domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientInvitationRevokeAsyncCompletion)completion;

#pragma mark - InvitationAccept

- (Gsa2DomainFull *)invitationAcceptSync:(NSString *)invitationId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientInvitationAcceptAsyncCompletion)(Gsa2DomainFull *domainFull, NSError *error);
- (void)invitationAcceptAsync:(NSString *)invitationId access:(NSString *)access completion:(Gsa2ClientInvitationAcceptAsyncCompletion)completion;

#pragma mark - CodeGet

- (NSString *)codeGetSync:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientCodeGetAsyncCompletion)(NSString *code, NSError *error);
- (void)codeGetAsync:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientCodeGetAsyncCompletion)completion;

#pragma mark - ControllersAdd

- (NSArray<Gsa2ControllerFull *> *)controllersAddSync:(NSArray<Gsa2ControllerFull *> *)controllersFull domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientControllersAddAsyncCompletion)(NSArray<Gsa2ControllerFull *> *controllersFull, NSError *error);
- (void)controllersAddAsync:(NSArray<Gsa2ControllerFull *> *)controllersFull domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientControllersAddAsyncCompletion)completion;

#pragma mark - ControllerRemove

- (BOOL)controllerRemoveSync:(NSString *)controllerId domainId:(NSString *)domainId access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientControllerRemoveAsyncCompletion)(NSError *error);
- (void)controllerRemoveAsync:(NSString *)controllerId domainId:(NSString *)domainId access:(NSString *)access completion:(Gsa2ClientControllerRemoveAsyncCompletion)completion;

#pragma mark - TokenIssue

- (Gsa2Token *)tokenIssueSync:(Gsa2Identity *)identity credential:(Gsa2Credential *)credential error:(NSError **)error;

typedef void (^Gsa2ClientTokenIssueAsyncCompletion)(Gsa2Token *token, NSError *error);
- (void)tokenIssueAsync:(Gsa2Identity *)identity credential:(Gsa2Credential *)credential completion:(Gsa2ClientTokenIssueAsyncCompletion)completion;

#pragma mark - TokenRevoke

- (BOOL)tokenRevokeSync:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientTokenRevokeAsyncCompletion)(NSError *error);
- (void)tokenRevokeAsync:(NSString *)access completion:(Gsa2ClientTokenRevokeAsyncCompletion)completion;

#pragma mark - CameraPlaylist

- (NSString *)cameraPlaylistSync:(NSString *)id key:(NSString *)key controller:(NSString *)controller access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientCameraPlaylistAsyncCompletion)(NSString *playlist, NSError *error);
- (void)cameraPlaylistAsync:(NSString *)id key:(NSString *)key controller:(NSString *)controller access:(NSString *)access completion:(Gsa2ClientCameraPlaylistAsyncCompletion)completion;

#pragma mark - CameraHeartbeat

- (BOOL)cameraHeartbeatSync:(NSString *)id controller:(NSString *)controller access:(NSString *)access error:(NSError **)error;

typedef void (^Gsa2ClientCameraHeartbeatAsyncCompletion)(NSError *error);
- (void)cameraHeartbeatAsync:(NSString *)id controller:(NSString *)controller access:(NSString *)access completion:(Gsa2ClientCameraHeartbeatAsyncCompletion)completion;

@end
