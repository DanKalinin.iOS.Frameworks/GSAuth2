//
//  Gsa2Schema.h
//  GSAuth2
//
//  Created by Dan Kalinin on 8/23/20.
//

#import "Gsa2Main.h"

#pragma mark - Account

@interface Gsa2Account : NseObject

@property NSString *id;
@property NSString *name;
@property NSString *password;

+ (instancetype)from:(GSA2Account *)sdkAccount;
- (GSA2Account *)to;

@end

@interface NSArray (Gsa2Account)

+ (instancetype)gsa2AccountFrom:(GList *)sdkAccounts;

@end

#pragma mark - Domain

@interface Gsa2Domain : NseObject

@property NSString *id;
@property NSString *owner;
@property NSString *name;

+ (instancetype)from:(GSA2Domain *)sdkDomain;
- (GSA2Domain *)to;

@end

@interface NSArray (Gsa2Domain)

+ (instancetype)gsa2DomainFrom:(GList *)sdkDomains;

@end

#pragma mark - Controller

@interface Gsa2Controller : NseObject

@property NSString *id;
@property NSString *mac;
@property NSString *model;
@property NSString *serial;

+ (instancetype)from:(GSA2Controller *)sdkController;
- (GSA2Controller *)to;

@end

@interface NSArray (Gsa2Controller)

+ (instancetype)gsa2ControllerFrom:(GList *)sdkControllers;

@end

#pragma mark - AccountDomain

@interface Gsa2AccountDomain : NseObject

@property NSString *account;
@property NSString *domain;
@property NSString *expiration;
@property NSInteger current;

@property NSDate *nsDateExpiration;

+ (instancetype)from:(GSA2AccountDomain *)sdkAccountDomain;

@end

@interface NSArray (Gsa2AccountDomain)

+ (instancetype)gsa2AccountDomainFrom:(GList *)sdkAccountDomains;

@end

#pragma mark - DomainController

@interface Gsa2DomainController : NseObject

@property NSString *domain;
@property NSString *controller;
@property NSString *key;
@property NSString *ip;
@property NSInteger port;
@property NSString *bssid;
@property NSInteger verified;

+ (instancetype)from:(GSA2DomainController *)sdkDomainController;
- (GSA2DomainController *)to;

@end

@interface NSArray (Gsa2DomainController)

+ (instancetype)gsa2DomainControllerFrom:(GList *)sdkDomainControllers;

@end

#pragma mark - Identity

@interface Gsa2Identity : NseObject

@property NSString *type;
@property NSString *value;
@property NSString *account;
@property NSString *controller;

+ (instancetype)from:(GSA2Identity *)sdkIdentity;
- (GSA2Identity *)to;

@end

@interface NSArray (Gsa2Identity)

+ (instancetype)gsa2IdentityFrom:(GList *)sdkIdentities;
- (GList *)gsa2IdentityTo;

@end

#pragma mark - Invitation

@interface Gsa2Invitation : NseObject

@property NSString *id;
@property NSString *domain;
@property NSString *name;
@property NSString *expiration;
@property NSInteger access;

@property NSDate *nsDateExpiration;

+ (instancetype)from:(GSA2Invitation *)sdkInvitation;
- (GSA2Invitation *)to;

@end

@interface NSArray (Gsa2Invitation)

+ (instancetype)gsa2InvitationFrom:(GList *)sdkInvitations;

@end

#pragma mark - Token

@interface Gsa2Token : NseObject

@property NSString *subject;
@property NSString *id;
@property NSString *access;
@property NSString *refresh;

+ (instancetype)from:(GSA2Token *)sdkToken;

@end

@interface NSArray (Gsa2Token)

+ (instancetype)gsa2TokenFrom:(GList *)sdkTokens;

@end

#pragma mark - Credential

@interface Gsa2Credential : NseObject

@property NSString *type;
@property NSString *value;

- (GSA2Credential *)to;

@end

#pragma mark - AccountFull

@interface Gsa2AccountFull : NseObject

@property Gsa2Account *account;
@property Gsa2AccountDomain *accountDomain;

+ (instancetype)from:(GSA2AccountFull *)sdkAccountFull;

@end

@interface NSArray (Gsa2AccountFull)

+ (instancetype)gsa2AccountFullFrom:(GList *)sdkAccountsFull;

@end

#pragma mark - DomainFull

@interface Gsa2DomainFull : NseObject

@property Gsa2Domain *domain;
@property Gsa2AccountDomain *accountDomain;

+ (instancetype)from:(GSA2DomainFull *)sdkDomainFull;

@end

@interface NSArray (Gsa2DomainFull)

+ (instancetype)gsa2DomainFullFrom:(GList *)sdkDomainsFull;

@end

#pragma mark - ControllerFull

@interface Gsa2ControllerFull : NseObject

@property Gsa2Controller *controller;
@property Gsa2DomainController *domainController;

+ (instancetype)from:(GSA2ControllerFull *)sdkControllerFull;
- (GSA2ControllerFull *)to;

@end

@interface NSArray (Gsa2ControllerFull)

+ (instancetype)gsa2ControllerFullFrom:(GList *)sdkControllersFull;
- (GList *)gsa2ControllerFullTo;

@end
